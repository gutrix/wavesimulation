﻿#if !defined(TRANSPARENT_WATER_MODULE)
#define TRANSPARENT_WATER_MODULE

#define FOAM_LIMITING_FACTOR 1.2f

sampler2D _CameraDepthTexture, _WaterBackground;
float4 _CameraDepthTexture_TexelSize;

float3 _WaterFogColor;
float _WaterFogDensity;

float3 ColorBelowWater (float4 screenPos, float foamingArea) {
    float2 uv = screenPos.xy;
    uv /= screenPos.w;
    #if UNITY_UV_STARTS_AT_TOP
        if (_CameraDepthTexture_TexelSize.y < 0) {
           uv.y = 1 - uv.y;
        }
    #endif

    float backgroundDepth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv));

    float surfaceDepth = UNITY_Z_0_FAR_FROM_CLIPSPACE(screenPos.z);

    float depthDifference = backgroundDepth - surfaceDepth;

    float3 backgroundColor = tex2D(_WaterBackground, uv).rgb;

	float fogFactor = exp2(-_WaterFogDensity * depthDifference);

	return lerp(_WaterFogColor, backgroundColor, fogFactor);
}

#endif