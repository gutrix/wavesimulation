Shader "Custom/WaveFormer" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_RampTex ("Ramp Tex", 2D) = "white" {}
		_RandTex ("Rand Tex", 2D) = "white" {}

		_RandTexWeight ("Random Texture Weighting",  Range(0,1)) = 0.5
		_MasterWaveSpeed ("Master Wave Speed",  Range(0,20)) = 3

		_FoamDepth ("Foam Depth",  Range(0,1)) = 0.2

		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0

		_WaterFogColor ("Water Fog Color", Color) = (0, 0, 0, 0)
		_WaterFogDensity ("Water Fog Density", Range(0, 2)) = 0.1

		_WaveA ("Wave A (dir, steepness, wavelength)", Vector) = (1,0,0.5,10)
		_WaveB ("Wave B", Vector) = (0,1,0.25,20)
		_WaveC ("Wave C", Vector) = (1,1,0.15,10)
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200

		GrabPass { "_WaterBackground" }

		CGPROGRAM
		#pragma surface surf Standard alpha vertex:vert finalcolor:ResetAlpha tessellate:tessEdge
		#pragma target 3.0

		#include "TransparentWaterModule.cginc"
		#include "Tessellation.cginc"

		sampler2D _MainTex, _RampTex, _RandTex;

		struct Input {
			float2 uv_MainTex;
			float4 screenPos;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		float4 _WaveA, _WaveB, _WaveC;
		float _EdgeLength = 10.0;
		float _RandTexWeight;
		float _MasterWaveSpeed;
		float _FoamDepth;

		float wave_speed = 1;

		void ResetAlpha (Input IN, SurfaceOutputStandard o, inout fixed4 color) {
			color.a = 1;
		}

		float4 tessEdge (appdata_full v0, appdata_full v1, appdata_full v2) {
    		return UnityEdgeLengthBasedTessCull(v0.vertex, v1.vertex, v2.vertex, _EdgeLength, 1);
		}

		float3 GerstnerWave (
			float4 wave, float3 p, float m, inout float3 tangent, inout float3 binormal
		) {
		    float steepness = (m) * wave.z;
		    float wavelength = wave.w;
		    float k = 2 * UNITY_PI / wavelength;
			float c = sqrt(9.8 / k);
			float2 d = normalize(wave.xy);
			float f = k * (dot(d, p.xz) - c * _Time.y / _MasterWaveSpeed);
			float a = steepness / k;

			tangent += float3(
				-d.x * d.x * (steepness * sin(f)),
				d.x * (steepness * cos(f)),
				-d.x * d.y * (steepness * sin(f))
			);
			binormal += float3(
				-d.x * d.y * (steepness * sin(f)),
				d.y * (steepness * cos(f)),
				-d.y * d.y * (steepness * sin(f))
			);
			return float3(
				d.x * (a * cos(f)),
				a * sin(f),
				d.y * (a * cos(f))
			);
		}

		void vert(inout appdata_full vertexData) {
			float3 gridPoint = vertexData.vertex.xyz;

			float rand = tex2Dlod(_RandTex, float4(vertexData.texcoord.xy + (_Time.y, _Time.y) / 100, 0.0, 0.0)).x;
			float ramp = tex2Dlod(_RampTex, float4(vertexData.texcoord.xy, 0.0, 0.0)).x;

			float local_height = (_RandTexWeight * rand + ramp) / 2;

			float3 tangent = float3(1, 0, 0);
			float3 binormal = float3(0, 0, 1);
			float3 p = gridPoint;
			p += GerstnerWave(_WaveA, gridPoint, local_height, tangent, binormal);
			p += GerstnerWave(_WaveB, gridPoint, local_height, tangent, binormal);
			p += GerstnerWave(_WaveC, gridPoint, local_height, tangent, binormal);
			float3 normal = normalize(cross(binormal, tangent));
			vertexData.vertex.xyz = p;
			vertexData.normal = normal;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;

			o.Emission = ColorBelowWater(IN.screenPos, _FoamDepth) * (1 - c.a);
		}
		ENDCG
	}
}