﻿/*
 * Copyright (c) 2019 IDEA Lab, Griffith University, All rights reserved.
 * 
 * Written by William Wayland 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NVIDIA.Flex;

// William Wayland 2019
// Flex script to create a reasonably sized water pool. 

public class WaterSpawner : MonoBehaviour
{

    public FlexContainer flexContainer;
    public FlexSourceAsset flexSourceAsset;

    public int numberOfSpawners = 1;
    private GameObject[] children;

    // Start is called before the first frame update
    void Start()
    {
        if(!flexContainer || !flexSourceAsset) {
            Debug.Log("Flex Container or source not defined.");
        }

        children = new GameObject[numberOfSpawners];

        for(int i = 0; i < numberOfSpawners; i++) {
            children[i] = new GameObject();
            children[i].transform.Translate(i/5, i%10, 2*(i%5) - 4);
            children[i].transform.parent = this.gameObject.transform;
            FlexSourceActor childsSourceActor = children[i].AddComponent<FlexSourceActor>();
            children[i].AddComponent<FlexFluidRenderer>();

            childsSourceActor.container = flexContainer;
            childsSourceActor.asset = flexSourceAsset;
            childsSourceActor.fluid = true;
            childsSourceActor.drawParticles = false;

            childsSourceActor.lifeTime = 1000;
            childsSourceActor.enabled = false;
            //children[i].Recreate();
            childsSourceActor.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
