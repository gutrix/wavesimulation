﻿/*
 * Copyright (c) 2019 IDEA Lab, Griffith University, All rights reserved.
 * 
 * Written by William Wayland 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// William Wayland | IdeaLab
// Locks the camera's playarea to the board's location. 

public class LockToBoard : MonoBehaviour
{

    // External references to the board, and VR camera.
    public GameObject board;
    public GameObject head;

    public bool standing;
    Vector3 pos = new Vector3(0, 0, 0);
    public const float standingHight = 0.4f;
    public const float layingHight = -0.6f;


    // Update is called once per frame
    void Update()
    {
        pos.x = transform.position.x;
        pos.z = transform.position.z;
        pos.y = board.transform.position.y + (standing ? standingHight : layingHight);

        ResetCameraPositionToBoard();

        gameObject.transform.position = pos;
    }

    // Rotate, then translate to match the boards position and direction.
    void ResetCameraPositionToBoard() {
        // Position
        Vector3 boardLocation = board.transform.position;
        Vector3 headsetLocation = head.transform.position;

        Vector3 diff = headsetLocation - boardLocation;
        diff.y = 0;
        pos -= diff;
    }
}
