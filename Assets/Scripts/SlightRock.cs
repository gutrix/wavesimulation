﻿/*
 * Copyright (c) 2019 IDEA Lab, Griffith University, All rights reserved.
 * 
 * Written by William Wayland 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// William Wayland | IdeaLab
// Basic rocking motion for an object. Used for the Flex wave generation.

public class SlightRock : MonoBehaviour
{
    // Start is called before the first frame update
    public float r;
    void Start()
    {
        r = 0;
    }

    // Update is called once per frame
    void Update()
    {
        r = Mathf.Sin(Time.timeSinceLevelLoad) / 50;
        this.gameObject.transform.Translate(new Vector3(0, r, 0));
        r = 0;
    }
}
