﻿/*
 * Copyright (c) 2019 IDEA Lab, Griffith University, All rights reserved.
 * 
 * Written by William Wayland 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Written by William Wayland! | IdeaLab
// Planned on using this to deform (attract) a mesh to create a wave. Didn't work.

public class MeshDeformerObject : MonoBehaviour
{

    public float effectiveRadius = 5;

    public float force = 10f;
    public float forceOffset = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
       StartCoroutine(StartScanning());
    }

    IEnumerator  StartScanning() {
        while(true) {
            // Can replace with WaitForSeconds if this ends up being a slow function to call, we'll see

            yield return new WaitForEndOfFrame();
            Activate();
        }
    }

    // Update is called once per frame
    void Activate()
    {
        Ray ray = new Ray(transform.position, -transform.up);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
            if(deformer) {
                Vector3 point = hit.point;

                float distance = Vector3.Distance(point, transform.position);
    
                // Distance here isn't between the two objects, its between the center of this object, and the point on the other object it's above
                // Important distinction between the too, important for a wide mesh (like water... hmmm....) 
                if(distance > effectiveRadius) return;

                Debug.DrawLine(point, transform.position);
                
                point += hit.normal * forceOffset;

                deformer.AddDeformingForce(point, force);
            }
        }
    }
}
