﻿/*
 * Copyright (c) 2019 IDEA Lab, Griffith University, All rights reserved.
 * 
 * Written by William Wayland 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// William Wayland | IdeaLab
// Calculates the force from a controller when it moves below a threshold (y=0)
// Pushes the board in the direction of the force.

public class ForceFromController : MonoBehaviour
{

    public GameObject left;
    public GameObject right;
    public float multiplier = 1.0f;

    private float threshold = 0.0f;
    private Vector3 lastPositionL;
    private Vector3 lastPositionR;
    private SurfBoardController sbController;

    // Start is called before the first frame update
    void Start()
    {
        sbController = GetComponent<SurfBoardController>();
        UpdateLastPosition();
    }

    // Update is called once per frame
    void Update()
    {   
        
        Vector3 forceL = CalculateForceForController(left, lastPositionL);
        Vector3 forceR = CalculateForceForController(right, lastPositionR);
        Vector3 totalForce = forceL + forceR;
        totalForce = new Vector3(totalForce.x, 0, totalForce.z);

        Debug.Log("Speed: " + totalForce.magnitude);

        totalForce *= 1000;
        totalForce *= 100;

        sbController.AddForce(totalForce);
        Debug.Log(totalForce);

        UpdateLastPosition();
    }

    Vector3 CalculateForceForController(GameObject con, Vector3 lastPos) {
        if(IsControllerBelowThreshold(con)) {
            Vector3 distance = con.transform.position - lastPos;
            return  (multiplier * distance) / (Time.deltaTime * Time.deltaTime);
        }
        return new Vector3(0, 0, 0);
    }

    void UpdateLastPosition() {
        lastPositionL = left.transform.position;
        lastPositionR = right.transform.position;
    }

    bool IsControllerBelowThreshold(GameObject con) {
        Debug.Log(con.transform.position.y - transform.position.y);
        return (con.transform.position.y - transform.position.y) < threshold;
    }
}
