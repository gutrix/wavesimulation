﻿/*
 * Copyright (c) 2019 IDEA Lab, Griffith University, All rights reserved.
 * 
 * Written by William Wayland 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// William Wayland | IdeaLab
// Controls the physics of the surf board, using its rigidbody and wheel colliders

public class SurfBoardController : MonoBehaviour
{

    private Rigidbody rd;
    public Wheels wheels;

    // Relitive Positions
    public Vector3 front;
    public Vector3 back;
    public Vector3 middle;

    // Start is called before the first frame update
    void Start()
    {
        rd = GetComponent<Rigidbody>();
        if(!rd) { 
            Debug.Log("Surfboard doesn't have a RD");
            return;
        }

        rd.velocity = -gameObject.transform.forward;

        UpdatePositions();

        gameObject.transform.Translate(new Vector3(0, 1, 0));
    }

    void UpdatePositions() {
        front = (wheels.fl.transform.localPosition + wheels.fr.transform.localPosition) / 2.0f;
        back  = (wheels.fl.transform.localPosition + wheels.fr.transform.localPosition) / 2.0f;
        middle = (front + back) / 2;
    }

    public void AddForce(Vector3 f) {
        if(Time.time < 1) return; // Don't rock the boat.
        rd.AddForce(f);
    }

    [System.Serializable]
    public class Wheels {
        // Each wheel, front right, front left, etc.
        public GameObject fr, fl, br, bl;
    }
}

