﻿/*
 * Copyright (c) 2019 IDEA Lab, Griffith University, All rights reserved.
 * 
 * Written by William Wayland 
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class simply translates its attached object at a speed to the end point,
// scaling to maxScale, until it has passed the percentage of its travel (percentSwitch)
// then it scales down to endScale.

public class WaveController : MonoBehaviour
{
    public GameObject wavePrefab;

    public Vector3 startScale = new Vector3(1, 1, 1);
    public Vector3 maxScale = new Vector3(1, 1, 1);
    public Vector3 endScale = new Vector3(1, 1, 1);
    public float angle = 0;
    public float period = 4;
    public float speed = 1;

    
    public float percentSwitch;
    public bool running;

    private Vector3 srtScale;
    private Vector3 srtPoint;
    private Vector3 endPoint;
    
    private float totalDistance;
    private float currentDistance;
    private float time;

    void Start() {
        endPoint = -gameObject.GetComponentInChildren<Transform>().localPosition;
        srtPoint = this.gameObject.transform.localPosition;
        srtScale = this.gameObject.transform.localScale;
        totalDistance = Vector3.Distance(srtPoint, endPoint);

        StartCoroutine(WaveSpawner());
    }

    IEnumerator WaveSpawner() {
        yield return new WaitForSeconds(1);
        
        while(running) {
            StartCoroutine(MoveWave());
            yield return new WaitForSeconds(period);
        }

        yield return 0;
    }

    IEnumerator MoveWave() {
        
        GameObject wave = Instantiate(wavePrefab, this.gameObject.transform);
        wave.transform.localScale = srtScale;
        MeshCollider meshCollider = wave.GetComponentInChildren<MeshCollider>();
       
        Vector3 current = wave.transform.localPosition;
       
        while(current != endPoint) {
            current = wave.transform.localPosition;
            
            Vector3 move = Vector3.MoveTowards(current, endPoint, Time.deltaTime * speed);

            wave.transform.localPosition = move;
            Debug.Log(move);
            Debug.Log(wave.transform.localPosition);
        
            // Waves scale
            currentDistance = Vector3.Distance(srtPoint, current);
            float percentTraveled = currentDistance / totalDistance;
        
            // Scaling Up - Linear Interpulation between the starting and max scaling
            if(percentTraveled <= percentSwitch) {
               wave.transform.localScale = Vector3.Lerp(srtScale, maxScale, percentTraveled / percentSwitch);
            } 
            // Scaling Down after the wave has passed max - Linear Interpulation between the max and end scaling
            else {
                wave.transform.localScale = Vector3.Lerp(maxScale, endScale, (percentTraveled - percentSwitch) / (1 - percentSwitch));
            }

            yield return new WaitForEndOfFrame();
        }

        Destroy(wave);
     
        yield return 0;
    }

    void Restart() {
        this.gameObject.transform.localPosition = srtPoint;
        this.gameObject.transform.localScale = srtScale;
    }
}
