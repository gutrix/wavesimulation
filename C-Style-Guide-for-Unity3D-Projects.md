# C# Style Guide for Unity3D Projects

---

# Tabs, Braces, Brackets, Newlines, Whitespace

- Use tabs. If you dont like the default tab width, you can change it in your code editor.
- Opening brace goes on the same line as the thing it's opening.
- Newlines to seperate code blocks. Use your own discretion to decide.
- Don't put whitespace between the bracket and what's in it. That's messy.
- Whitespace between symbols. Makes it easier to read.
- Add a space at the start of comments. Again, easier to read.
- Lines should never be longer than XXX.

**BAD:**

    [//Returns](//returns) true if positive
    bool isPositive( int i )
    {
    
       if( i<0 )
       {
          print( "i is negative" ) ;
          return false;
       } else
       {
          return true;
       }
    }

**GOOD:**

    // Returns true if positive
    bool isPositive(int i) {
    
    	if(i < 0) {
    		print("i is negative");
    		return false;
    	}
    
    	else
    		return true;
    }

## Switch Cases

Switch-statements come with `default` case by default (heh). When your code is written correctly, it should never reach this part. Never include the `default` case.

**BAD:**

    switch (variable) { 
    	case 1: 
    		break; 
    	case 2: 
    		break; 
    	default: 
    		break;
    }

**GOOD:**

    switch (variable) { 
    	case 1: 
    		break; 
    	case 2: 
    		break;
    }

# Namespaces

Namespaces are all **PascalCase**, multiple words concatenated together, without hypens ( - ) or underscores ( _ ):

**BAD:**

    com.raywenderlich.fpsgame.hud.healthbar

**GOOD:**

    RayWenderlich.FPSGame.HUD.Healthbar

---

# Classes, Interfaces, Fields, Methods & Parameters

*Classes* and *Interfaces* are written in **PascalCase**. For example 'RadialSlider'.

All interfaces should be prefaced with the letter **I**.

*Methods* and are written in **PascalCase**. For example 'DoSomething()'.

*Non-Static Fields* and *Parameters* are written in **camelCase**.

Only *Static Fields* should be written in **PascalCase**.

**BAD:**

    public static int theAnswer = 42;
    private int _myPrivateVariable
    void doSomething(Vector3 Location);

**GOOD:**

    public static int TheAnswer = 42;
    private int myPrivateVariable;
    void DoSomething(Vector3 location);

---

# Delegates and Events

*Delegates* are written in **PascalCase**, and must have the suffix **EventHandler** or **Callback** for delegates that are not event handlers.

*Events* should have the prefix **On.**

**BAD:**

    public delegate Click();
    public delegate Render();
    public static event CloseCallback Close;

**GOOD:**

    public delegate ClickEventHandler();
    public delegate void RenderCallback();
    public static event CloseCallback OnClose;

# Language

Regardless of acronyms, stick to the appropriate titling style.

Use US English spelling for variable names.

**BAD:**

    XMLHTTPRequest
    String URL
    findPostByID
    string colour = "red";

**GOOD:**

    XmlHttpRequest
    String url
    findPostById
    string color = "red";

# **Declarations**

Access level modifiers should be explicitly defined for classes, methods and member variables.

Cluster declariations based on access level.

**BAD:**

    public GameObject grampaJoe;
    protected AudioSource jellySound;
    private bool isAlive;
    protected AudioSource potatoSound;
    public GameObject GrandmaStacy;
    private int counter;

**GOOD:**

    public GameObject grampaJoe;
    public GameObject GrandmaStacy;
    
    protected AudioSource potatoSound;
    protected AudioSource jellySound;
    
    private int counter;
    private bool isAlive;

*Fields* and *variables* should be declared one per line, unless in specific contexts where it makes sense. If you don't know those contexts, one declaration per line.

**BAD:**

    string username, twitterHandle;

**GOOD:**

    string username;
    string twitterHandle;
    int x, y, z;

## **Classes**

Exactly one class per source file, except for inner classes where scoping appropriate. More accurately, only one monobehavior per source file.

# **Copyright Statement**

The following copyright statement should be included at the top of every source file:

    /*
     * Copyright (c) 2017 IDEA Lab, Griffith University, All rights reserved.
     * 
     * Written by Firstname Lastname
     * Modified by Firstname Lastname (DATE)
     */