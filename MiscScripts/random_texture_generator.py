from PIL import Image
import numpy as np
import sys
from noise import pnoise2, snoise2

size = 1000
octaves = 1
freq = 16.0 * octaves

dimensions = (size, size)
im = Image.new('L', dimensions)

for y in range(size):
	for x in range(size):
		im.putpixel((x, y), int(snoise2(x / freq, y / freq, octaves) * 127.0 + 128.0))


im.save('test_1.png', 'png')

